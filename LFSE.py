#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re #Regular expressions

#Anzahl der d-Elektronen
d_el = {'Ti':4,
         'V':5,
        'Cr':6,
        'Mn':7,
        'Fe':8, 
        'Co':9, 
        'Ni':10,
        'Cu':11,
        'Zn':12
        }

#Orbitalaufspaltung der Ionen im Oktaederfeld high-spin (hs)
#'unp' = Zahl der ungeparten Elektronen
d_split_o_hs = {1:{'t2g':1, 'eg':0, 'unp':1},
                2:{'t2g':2, 'eg':0, 'unp':2},
                3:{'t2g':3, 'eg':0, 'unp':3},
                4:{'t2g':3, 'eg':1, 'unp':4},
                5:{'t2g':3, 'eg':2, 'unp':5},
                6:{'t2g':4, 'eg':2, 'unp':4},
                7:{'t2g':5, 'eg':2, 'unp':3},
                8:{'t2g':6, 'eg':2, 'unp':2},
                9:{'t2g':6, 'eg':3, 'unp':1},
               10:{'t2g':6, 'eg':4, 'unp':0},
             }
             
#Orbitalaufspaltung der Ionen im Oktaederfeld low-spin (ls)
#'unp' = Zahl der ungeparten Elektronen
d_split_o_ls = {1:{'t2g':1, 'eg':0, 'unp':1},
                2:{'t2g':2, 'eg':0, 'unp':2},
                3:{'t2g':3, 'eg':0, 'unp':3},
                4:{'t2g':4, 'eg':0, 'unp':2},
                5:{'t2g':5, 'eg':0, 'unp':1},
                6:{'t2g':6, 'eg':0, 'unp':0},
                7:{'t2g':6, 'eg':1, 'unp':1},
                8:{'t2g':6, 'eg':2, 'unp':2},
                9:{'t2g':6, 'eg':3, 'unp':1},
               10:{'t2g':6, 'eg':4, 'unp':0},
             }
             
#Orbitalaufspaltung der Ionen im Tetraederfeld high-spin (ls)
#'unp' = Zahl der ungeparten Elektronen
d_split_t_hs = {1:{'e':1, 't2':0, 'unp':1},
                2:{'e':2, 't2':0, 'unp':2},
                3:{'e':2, 't2':1, 'unp':3},
                4:{'e':2, 't2':2, 'unp':4},
                5:{'e':2, 't2':3, 'unp':5},
                6:{'e':3, 't2':3, 'unp':4},
                7:{'e':4, 't2':3, 'unp':3},
                8:{'e':4, 't2':4, 'unp':2},
                9:{'e':4, 't2':5, 'unp':1},
               10:{'e':4, 't2':6, 'unp':0},
             }

def parse_sum_formula(sum_formula):
    #Parser für Summenformel
    #Regular expression für Element, Anzahl und Ladung
    #Formel muss in der Art 'Element1(Ladung1)Anzahl1Element2(Ladung2)Anzahl2... sein
    #z.B. Fe(2+)2(Co3+)3O(2-)4
    #
    #pattern = r'([A-Z][a-z]*)(\d*)(\((\d?[-+])\))?'
    pattern = r'([A-Z][a-z]*)(\((\d?[-+])\))?(\d*)'
    # findall für alle Treffer in der Formel
    matches = re.findall(pattern, sum_formula)
    
    # Dictionary um die Werte zu speichern
    elements = {}
    
    # Durch Treffer iterieren und Dict füllen
    for match in matches:
        element, _, charge_str, count = match
        count = int(count) if count else 1  # Wenn count leer, setze es zu 1
        ion = element + _
        # Charge separat parsen
        if charge_str == '+':
            charge_str = "1+"
        elif charge_str == '-':
            charge_str= "1-"

            
        charge = int(charge_str[:-1]) if charge_str else 0  # Nummer extrahieren -> int
            
        if charge_str == '': charge_str='0'
        #Dict füllen
        try:
            elements[ion] =    {'element': element,
                                'count': elements.get(ion, {}).get('count', 0) + count,
                                'charge': charge, 
                                'charge_str': charge_str, 
                                'd_electrons': d_el[element]-charge, 
                                'O_hs':d_split_o_hs[d_el[element]-charge],
                                'O_ls':d_split_o_ls[d_el[element]-charge],
                                'T_hs':d_split_t_hs[d_el[element]-charge],
                                }
        #Key error wenn nicht in 'd_el' Dict
        #dann kein d-Element
        except KeyError:
            elements[ion] =     {'element': element,
                                'count': elements.get(ion, {}).get('count', 0) + count,
                                'charge': charge, 
                                'charge_str': charge_str, 
                                'd_electrons': 0,
                                }
    
    return elements

def delta_O(ion):
    #Berechne delta_o für verschiedene Fälle (Tet <> Okt, hs <> ls)
    t2g_hs = ion.get('O_hs').get('t2g')
    eg_hs = ion.get('O_hs').get('eg')
    t2g_ls = ion.get('O_ls').get('t2g')
    eg_ls = ion.get('O_ls').get('eg')
    t2_hs = ion.get('T_hs').get('t2')
    e_hs = ion.get('T_hs').get('e')
    delta_o_hs = (-t2g_hs * 2/5) + (eg_hs * 3/5)
    delta_o_ls = (-t2g_ls * 2/5) + (eg_ls * 3/5)
    delta_o_te = ((-e_hs * 3/5) + (t2_hs * 2/5)) * 4/9
    return delta_o_hs, delta_o_ls, delta_o_te
    

chemical_formula_with_charges = input('Eingabe der Summenformel in der Form ' +
                                      'A(n+)xB(m+)yX(o-)z\n' +
                                      'z.B. Zn(2+)Cr(3+)O(2-)4:\n')

if chemical_formula_with_charges == '':
    chemical_formula_with_charges = "Zn(2+)Cr(3+)2O(2-)4"
    print("Zn(2+)Fe(3+)2O(2-)4")
    
#Eingabe 'parsen'
elements = parse_sum_formula(chemical_formula_with_charges)

#Tabelle mit Ausgabe der Daten aus der Formel
#e- ist die Anzahl der ungepaarten Elektronen
print('')
print("{:<6} {:<5} {:<5} {:<5} {:<16} {:<15} {:<15}".format('Ion', 'Anz.', 'Lad.', 'd-El.',
                                                     'O(hs)[t₂g,eg,e-]',
                                                     'O(ls)[t₂g,eg,e-]',
                                                     'T(hs)[e,t₂,e-]'))
for ion, data in elements.items():
    count = data['count']
    charge_str = data['charge_str']
    d_el = data['d_electrons']
    if 'O_hs' in data:
        O_hs = list(data['O_hs'].values())
        O_ls = list(data['O_ls'].values())
        T_hs = list(data['T_hs'].values())
    else:
        #O_hs etc. nicht angeben für nicht-d-Elemente
        O_hs = '-'
        O_ls = '-'
        T_hs = '-'
    print("{:<6} {:<5} {:<5} {:<5} {:<16} {:<16} {:<15}".format(ion, count, charge_str, 
                                                d_el, str(O_hs),str(O_ls),
                                                str(T_hs)))
                                                
print('')

#Tabelle mit Ausgabe delta_o für einzelne Ionen
print("{:<10} {:<10} {:<10} {:<10}".format('Ion', '𝛥ₒ(hs)', '𝛥ₒ(ls)', '𝛥ₒTet(hs)'))
for ion, data in elements.items():
    if 'O_hs' in data:
        #Berechne delta_o für einzelne Ionen
        delta_o_hs, delta_o_ls, delta_o_te = delta_O(elements[ion])
        #füge delta_o dem Dict hinzu
        elements[ion].update({'delta_o_hs':delta_o_hs,
                              'delta_o_ls':delta_o_ls,
                              'delta_o_te':delta_o_te,
                               })
    else:
        #setze delta_o =0 für nicht-d-Elemente
        delta_o_hs = 0
        delta_o_ls = 0
        delta_o_te = 0
    print(f"{ion:<6} {f'{delta_o_hs:.3f}':>9} {f'{delta_o_ls:.3f}':>10} {f'{delta_o_te:.3f}':>10}")

#Berechne LFSE in delta_o für Spinell AB2X4, sofern möglich
#Das erste Element ist A, das zweite ist B
#
#Normaler Spinell [A]tet[B2]okt(X4)
#Inverser Spinell [B]tet[AB]okt(X4)
if len(elements) > 1:
    A = list(elements.keys())[0]
    B = list(elements.keys())[1]
   

    if 'O_hs' in elements[A] and 'O_hs' in elements[B]:
        #Ausgabe LFSE für Spinell
        print('')
        print('Berechnung der LFSE (in 𝛥ₒ) des Spinells AB₂X₄ mit:')
        print('A:', A)
        print('B:', B)
        print('')
        lfse_nrm_hs = elements[A].get('delta_o_te') + 2 * elements[B].get('delta_o_hs')
        lfse_inv_hs = elements[B].get('delta_o_te') + elements[A].get('delta_o_hs') + elements[B].get('delta_o_hs')
        lfse_nrm_ls = elements[A].get('delta_o_te') + 2 * elements[B].get('delta_o_ls')
        lfse_inv_ls = elements[B].get('delta_o_te') + elements[A].get('delta_o_ls') + elements[B].get('delta_o_ls')
        print(f'Normaler Spinell (hs) [{A}]ₜ[{B}₂]ₒX₄: {lfse_nrm_hs:.2f}')
        print(f'Inverser Spinell (hs) [{B}]ₜ[{A}{B}]ₒX₄: {lfse_inv_hs:.2f}')
        print('')
        print(f'Normaler Spinell (ls) [{A}]ₜ[{B}₂]ₒX₄: {lfse_nrm_ls:.2f}')
        print(f'Inverser Spinell (ls) [{B}]ₜ[{A}{B}]ₒX₄: {lfse_inv_ls:.2f}')